﻿#include <opencv2/core/core.hpp> // Basic OpenCV structures (cv::Mat, Scalar)
#include <opencv2/highgui/highgui.hpp> // window I/O
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/imgproc/imgproc.hpp> // Blur
#include <vector>
#include <iostream>
#include <time.h>

using namespace cv;
using namespace std;

string windowName = "PAPIER | KAMIEN | NOZYCE";

string kamienCascadeFile = "cascades/kamien.xml";
string papierCascadeFile = "cascades/papier.xml";
string nozyceCascadeFile = "cascades/nozyce.xml";
string komputer;
string gracz;
bool start = true;
int komputerWynik = 0;
int graczWynik = 0;

CascadeClassifier kamienCascade, papierCascade, nozyceCascade; //załadowanie gotowego klasyfikatora

vector<Rect> kamien, papier, nozyce;

int sprawdzPliki()
{
	if (!kamienCascade.load(kamienCascadeFile))
	{
		cout << "Nie znaleziono pliku: " << kamienCascadeFile << ".";
		return -1;
	}
	else cout << "Wczytano plik poprawnie: " << kamienCascadeFile << "." << endl;

	if (!papierCascade.load(papierCascadeFile))
	{
		cout << "Nie znaleziono pliku: " << papierCascadeFile << ".";
		return -1;
	}
	else cout << "Wczytano plik poprawnie: " << papierCascadeFile << "." << endl;

	if (!nozyceCascade.load(nozyceCascadeFile))
	{
		cout << "Nie znaleziono pliku: " << nozyceCascadeFile << ".";
		return -1;
	}
	else cout << "Wczytano plik poprawnie: " << nozyceCascadeFile << "." << endl;
}

void losujKomputer()
{
	start = false;
	int losowa = (rand() % (3 + 1 - 1)) + 1;
	switch (losowa)
	{
	case 1:
		komputer = "kamien";
		break;
	case 2:
		komputer = "papier";
		break;
	case 3:
		komputer = "nozyce";
		break;
	}
}

void sprawdzWynik()
{
	cout << "Gracz " + gracz << " - " << komputer + " Komputer" << endl;

	//REMISY
	if (gracz == "kamien" && komputer == "kamien")
	{
		cout << "Remis" << endl << endl;
	}
	if (gracz == "papier" && komputer == "papier")
	{
		cout << "Remis" << endl << endl;
	}
	if (gracz == "nozyce" && komputer == "nozyce")
	{
		cout << "Remis" << endl << endl; 
	}

	//WYGRYWA GRACZ
	if (gracz == "kamien" && komputer == "nozyce")
	{
		cout << "Wygrywa gracz" << endl << endl;
		graczWynik++;
	}
	if (gracz == "papier" && komputer == "kamien")
	{
		cout << "Wygrywa gracz" << endl << endl;
		graczWynik++;
	}
	if (gracz == "nozyce" && komputer == "papier")
	{
		cout << "Wygrywa gracz" << endl << endl;
		graczWynik++;
	}

	//WYGRYWA KOMPUTER
	if (gracz == "kamien" && komputer == "papier")
	{
		cout << "Wygrywa komputer" << endl << endl;
		komputerWynik++;
	}
	if (gracz == "papier" && komputer == "nozyce")
	{
		cout << "Wygrywa komputer" << endl << endl;
		komputerWynik++; 
	}
	if (gracz == "nozyce" && komputer == "kamien")
	{
		cout << "Wygrywa komputer" << endl << endl;
		komputerWynik++;
	}
}

void detectKamien(Mat frame) // wykrywanie gestu za pomoca pliku cascade/plik.xml 
{		// sprawdzanie czy plik zostal zaladowany poprawnie
	kamienCascade.detectMultiScale(frame, kamien, 1.1, 2, 0 | CV_HAAR_SCALE_IMAGE, Size(20, 20));
	for (unsigned i = 0; i < kamien.size(); i++)
	{
		Rect kamienDetected(kamien[i]);
		rectangle(frame, kamienDetected, Scalar( 255, 255, 255 ), 2, 2, 0  ); // kolor w RGB
	}
}

void detectPapier(Mat frame)
{
	papierCascade.detectMultiScale(frame, papier, 1.1, 2, 0 | CV_HAAR_SCALE_IMAGE, Size(40, 40));
	for (unsigned i = 0; i < papier.size(); i++)
	{
		Rect papierDetected(papier[i]);
		rectangle(frame, papierDetected, Scalar( 0, 0, 0 ), 2, 2, 0  );
	}
}

void detectNozyce(Mat frame)
{
	nozyceCascade.detectMultiScale(frame, nozyce, 1.1, 2, 0 | CV_HAAR_SCALE_IMAGE, Size(40, 40));
	for (unsigned i = 0; i < nozyce.size(); i++)
	{
		Rect nozyceDetected(nozyce[i]);
		rectangle(frame, nozyceDetected, Scalar( 255, 0, 0 ), 2, 2, 0  );
	}
}

void redText(Mat frame, string text, int position_x, int position_y)
{
	int fontFace = CV_FONT_HERSHEY_SIMPLEX;
	double fontScale = 1;
	int thickness = 2;

	Point textPosition(position_x, position_y); // pozycja napisu 
	putText(frame, text, textPosition, fontFace, fontScale, Scalar(0, 0, 255), thickness, 8); // kolor
}

void blueText(Mat frame, string text, int position_x, int position_y)
{
	int fontFace = CV_FONT_HERSHEY_SIMPLEX;
	double fontScale = 1;
	int thickness = 2;

	Point textPosition(position_x, position_y);
	putText(frame, text, textPosition, fontFace, fontScale, Scalar(255, 0, 0), thickness, 8);
}

void wykryteGesty()
{
	if (nozyce.empty() && kamien.empty() && papier.empty()) // jeśli nie wykrywa gestu
	{
		gracz = "brak gestu";
	}
	else if (!papier.empty()) {
		gracz = "nozyce";
	}
	else if (!kamien.empty()) {
		gracz = "kamien";
	}
	else if (!nozyce.empty()) {
		gracz = "papier";
	}	
}

int main(int argc, char** argv)
{
	srand(time(NULL));
	bool continueCapture = true;
	Mat frame;

	sprawdzPliki(); 

	namedWindow(windowName, CV_WINDOW_AUTOSIZE);
	VideoCapture cap(0);

	cap.set(CV_CAP_PROP_FRAME_WIDTH, 400); // ustawianie rozmiaru okna 
	cap.set(CV_CAP_PROP_FRAME_HEIGHT, 300);

	double w = cap.get(CV_CAP_PROP_FRAME_WIDTH); // pobieranie rozmiaru okna
	double h = cap.get(CV_CAP_PROP_FRAME_HEIGHT);
	cout << "\n\n";
	cout << "	#########___GRA: PAPIER KAMIEN NOZYCE___#########" << endl;
	cout << "	#                                               #" << endl;
	cout << "	#	Rozdzielczosc kamery: " << w << " x " << h <<"         #" << endl;
	cout << "	#                                               #" << endl;
	cout << "	#      -  ESC - zakoncz                         #" << endl;
    cout << "	#      -  SPACJA - graj                         #" << endl;
	cout << "	#  1. Ustaw reke w kadrze.                      #" << endl;
	cout << "	#  2. Sprawdz czy poprawnie wykrywa:            #" << endl;
	cout << "	#          kamien, papier i nozyce.             #" << endl;
	cout << "	#  3. Wcisnij SPACJE do rozpoczecia gry         #" << endl;
	cout << "	#################################################" << endl;
	cout << "\n\n" << endl;

	int iLowH = 0;
	int iHighH = 71;

	int iLowS = 79;
	int iHighS = 255;

	int iLowV = 0;
	int iHighV = 255;


	while (continueCapture)
	{
		Mat mirror;
		Mat win = imread("winer.jpg", 1);
		Mat lose = imread("loser.jpg", 1);

		if (cap.read(frame))
		{
			Mat imgHSV;

			cvtColor(frame, imgHSV, COLOR_BGR2HSV); //konwertuje kolory do HSV

			Mat imgThresholded;

			inRange(imgHSV, Scalar(iLowH, iLowS, iLowV), Scalar(iHighH, iHighS, iHighV), imgThresholded);

			//imshow("Thresholded Image", imgThresholded);

			Mat roi(imgThresholded, Rect(2, 2, 25, 25)); 


			int zegar = 0;	
			while (zegar > 15)
			{
					losujKomputer();
					sprawdzWynik();
					cout << zegar;
					start = true;
					zegar = 0;
			}
		}

		detectKamien(frame); // funkcje odpowiedzialne za odczytywanie gestu 
		detectPapier(frame); // z pliu cascade
		detectNozyce(frame);

		flip(frame, mirror, 1); // obraca obraz z kamery na normalny z naszej perspektywy

		wykryteGesty();	
		blueText(mirror, gracz, 10, 30);

		string gr;
		string komp;
		string tmp;

		_itoa(graczWynik, (char*)tmp.c_str(), 10); // przechowywanie aktualnego wyniku
		gr = tmp.c_str();
		_itoa(komputerWynik, (char*)tmp.c_str(), 10);
		komp = tmp.c_str();

		redText(mirror, "Gracz: " + gr, 10, 250);
		redText(mirror, "Komputer: " + komp, 10, 275);
		
		int k = waitKey(1); // wyswietlanie klatki w ms

		if (graczWynik == 3) {
			mirror = win;
			if (k == 32) { // jesli zostanie wcisniety znak spacji (ascii), to program zostaje przerwany
				continueCapture = false;
			}
		}
		else if (komputerWynik == 3) {
			mirror = lose;
			if (k == 32) {
				continueCapture = false;
			}
		}
		imshow(windowName, mirror); // okno wyswietlajace po zakończeniu gry plik.jpg


		if (k == 32) { // kontynuacja po wykryciu gestu 
			losujKomputer();
			sprawdzWynik();
		}
		else if (k == 27) { // znak ESC (ascii)
			continueCapture = false;
		}
	}
	return 0;
}
